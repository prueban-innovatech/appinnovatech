import { Routes, Route } from "react-router-dom";
import NavBar from "./components/ResponsiveAppBar;.jsx";
import Landing from "./components/Landing.jsx";
import Home from "./components/Home.jsx";
import "./App.css";

function App() {
  return (
    <div className="App">
      <NavBar />
      <Routes>
        <Route path="*" element={<h2>No existe esa ruta</h2>} />
        <Route path="/" element={<Landing/>} />
        <Route path="/home" element={<Home/>} />
      </Routes>
    </div>
  );
}

export default App;
