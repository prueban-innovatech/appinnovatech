import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Update from './Update';

export default function AlertDialog({data, setUserData, userData}) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <Button variant="outlined" onClick={handleClickOpen}>
        Actualizar
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {`Estas seguro que quieres actualizar a ${data.title}?`}
        </DialogTitle>
        <DialogContent>
           <Update setOpen={setOpen} data={data} setUserData={setUserData}  userData={userData}/>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
}
