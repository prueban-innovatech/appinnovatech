import React from "react";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useForm } from "../utility/hooks";

const defaultTheme = createTheme();

export default function Update({ setOpen, data, setUserData, userData }) {
  function loginUserCallback() {
    updateUSer();
  }

  const { onChange, onSubmit, values } = useForm(loginUserCallback, {
    title: "",
    body: "",
    userId: "",
  });

  const updateUSer = () => {
    values.userId = data.userId;
    if (!values.title || !values.body) {
      return alert("Faltan campos por llenar");
    }

    fetch("https://jsonplaceholder.typicode.com/posts/1", {
      method: "PUT",
      body: JSON.stringify(values),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        userData = userData?.map((user) =>
          user.userId === json.userId ? json : user
        );
        setUserData(userData);
        setOpen(false);
      });
  };
  
  return (
    <ThemeProvider theme={defaultTheme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Box component="form" noValidate onSubmit={onSubmit} sx={{ mt: 3 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="given-name"
                  name="title"
                  required
                  fullWidth
                  id="title"
                  label="Nombre"
                  value={values?.title}
                  onChange={onChange}
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  fullWidth
                  id="body"
                  label="Descripción"
                  value={values?.body}
                  name="body"
                  onChange={onChange}
                  autoComplete="family-name"
                />
              </Grid>
            </Grid>
            <Button type="submit" variant="outlined" color="primary" sx={{ mt: 3, mb: 2 }}>
              Actualizar usuario
            </Button>
            <Button onClick={()=> setOpen(false)} variant="outlined" color="error" sx={{ mt: 3, mb: 2 }}>
              Cancelar
            </Button>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}
