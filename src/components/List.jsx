import React, { useState } from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";
import AlertDialog from "./Dialog";
import Alert from "@mui/material/Alert";
import CheckIcon from "@mui/icons-material/Check";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

export default function CustomizedTables({ userData, setUserData }) {
  const [showAlert, setShowAlert] = useState(false);
  const [ name, setName] = useState("");

  const handleDelete = (userId, name) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${userId}`, {
      method: "DELETE",
    })
      .then((response) => response.json())
      .then((json) => {
        userData = userData?.filter((user) => user.userId !== userId);
        setUserData(userData);
        setName(name)
        setShowAlert(true);
        setTimeout(() => setShowAlert(false), 3000);
      });
  };

  return (
    <TableContainer component={Paper}>
      {showAlert && (
        <Alert icon={<CheckIcon fontSize="inherit" />} severity="success">
          La fila {name} fue eliminada correctamente
        </Alert>
      )}
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Nombre</StyledTableCell>
            <StyledTableCell align="right">Descripción</StyledTableCell>
            <StyledTableCell align="right">Acciones</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {userData?.length > 0 &&
            userData?.map((row) => (
              <StyledTableRow key={row.userId}>
                <StyledTableCell component="th" scope="row">
                  {row.title}
                </StyledTableCell>
                <StyledTableCell align="right">{row.body}</StyledTableCell>
                <StyledTableCell align="right">
                  <AlertDialog
                    data={row}
                    userData={userData}
                    setUserData={setUserData}
                  />
                  <Button
                    variant="outlined"
                    color="error"
                    onClick={() => handleDelete(row.userId, row.title)}
                  >
                    Eliminar
                  </Button>
                </StyledTableCell>
              </StyledTableRow>
            ))}
          {userData?.length === 0 && <h2>Sin datos</h2>}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
