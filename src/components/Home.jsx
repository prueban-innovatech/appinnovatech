import React, { useEffect, useState } from 'react';
import { Grid } from '@mui/material'
import CustomizedTables from './List';
import Form from './Form';
import { getUser } from '../utility/getUser.js'


export default function Home() {
    const [userData, setUserData] = useState([]);

    useEffect(( )=>{
        getUser().then(user => {
            setUserData(user?.slice(100));
          });
    },[])

  return (
      <Grid container style={{padding:"20px"}}>
        <Grid item xs={12}sm={6}>
        <CustomizedTables setUserData ={setUserData} userData={userData}/>
        </Grid>
        <Grid item xs={12} sm={6}>
        <Form setUserData={setUserData} userData={userData}/>
        </Grid>
      </Grid>
  );
}